#!/bin/bash
# SPDX-License-Identifier: MIT

###############
# configuration
###############

# working container name
CONTAINER=contcompl-working

# final image name
IMAGENAME=myapp

# Debian packages to be installed
PKGS=sl

# local directory with my app's sources
LOCALAPPDIR=./app

# container directory where sources should be placed
CONTAINERAPPDIR=/app

# entrypoint command
ENTRYPOINT=/app/app.sh

###############
# build steps
###############

# stop on error
set -e

# create local output directory for sources
mkdir -p ./output/app

# start with Debian latest
buildah from --name=${CONTAINER} debian

# update / upgrade -- TODO: is this actually needed?
buildah run ${CONTAINER} -- apt update -y
buildah run ${CONTAINER} -- apt upgrade -y

# install requested packages
buildah run ${CONTAINER} -- apt install -y ${PKGS}

# copy get-sources script into the working container
buildah copy ${CONTAINER} ./get-sources.sh /root/get-sources.sh

# run it
buildah run ${CONTAINER} -- /root/get-sources.sh

# remove get-sources script
buildah run ${CONTAINER} -- rm /root/get-sources.sh

# mount the container and copy out the sources tarball and packages list
# via unshare so that we can do it rootless
buildah unshare --mount MOUNTPOINT=${CONTAINER} sh -c 'cp ${MOUNTPOINT}/root/* ./output'

# and remove those files
buildah run ${CONTAINER} -- rm /root/sources.tar.gz
buildah run ${CONTAINER} -- rm /root/packages.list

# copy our own app's sources into the container
buildah copy ${CONTAINER} ${LOCALAPPDIR} ${CONTAINERAPPDIR}

# and also into the sources dir
cp -r ${LOCALAPPDIR} ./output

# configure the entrypoint
buildah config --entrypoint ${ENTRYPOINT} ${CONTAINER}

# and finalize the container
buildah commit ${CONTAINER} ${IMAGENAME}

echo "Done. Run container with: podman run --rm ${IMAGENAME}"

