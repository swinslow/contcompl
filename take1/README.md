SPDX-License-Identifier: MIT

First attempt
=============

- Just uses shell scripting calls to buildah.
- Configure via defines at top of make-container.sh

Requirements
------------
* [buildah](https://github.com/containers/buildah)

These scripts are really not much more than a wrapper around
buildah, taking advantage of debian and apt to retrieve
sources.

Limitations
-----------
This is meant only as a proof of concept and therefore it makes
many assumptions / has many limitations, in particular it:

* assumes you are using debian:latest as your base image.
* doesn't work with Dockerfiles; the "build" process, such as
  it is, is limited to installing debian packages and then
  copying your own application's sources into the container.

How to run
----------
1. Edit `./make-container.sh` to configure for your application,
   in particular the following:
   - PKGS: the names of the Debian package(s) that you want to
     have installed, separated by spaces (this is fed directly
     into `apt install -y`)
   - LOCALAPPDIR: path to where your app's sources are stored
   - CONTAINERAPPDIR: path to where your app's sources should
     be copied into in the container
   - ENTRYPOINT: command to be executed when container is run

2. Run `./make-container.sh`

3. The source code for the installed debian packages, as well
   as a list of the packages and versions, will be saved in
   the `output` directory, along with a copy of your
   application's own sources.

A sample application is included in ./app/app.sh, which is a
bash script that does the critically important task of
displaying a steam locomotive and then saying hi.

DO NOT run the `get-sources.sh` script on your own system. It
is copied into and run from within the container as part of
the `make-container.sh` script.

