Building a compliant container -- proof of concept
==================================================

Current methods for building container images make it easy to
do the wrong thing from a licensing perspective. For details
on why this is the case, see [this whitepaper from Armijn Hemel](https://www.linuxfoundation.org/publications/2020/04/docker-containers-for-legal-professionals/).

This repo is an experiment in trying to solve one aspect of
the compliance challenges: obtaining (and being able to provide)
corresponding sources for dependencies that are incorporated
into the container image.

The experiments in this repo make extensive use of [buildah](github.com/containers/buildah)
for all container operations.

Experiments
-----------
1. [take1](./take1): Build a container and get its sources,
   via a configurable bash script
2. [take2](./take2): Same, but in Golang

License
-------
[MIT](LICENSE.txt)

SPDX-License-Identifier: MIT

