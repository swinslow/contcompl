// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
	"os"
	"path"
	"time"

	"github.com/spdx/tools-golang/spdx"
	"github.com/spdx/tools-golang/tvsaver"
)

func makeSPDXDocument(app AppDetails) (*spdx.Document2_1, error) {
	// parse and get packages list
	pkglistPath := path.Join(app.OutputDir, "packages.list")
	pkgs, err := getPackages(pkglistPath)
	if err != nil {
		return nil, err
	}

	// get created time for doc
	location, _ := time.LoadLocation("UTC")
	created := time.Now().In(location).Format("2006-01-02T15:04:05Z")

	// start creating SPDX document
	doc := &spdx.Document2_1{
		Packages:        map[spdx.ElementID]*spdx.Package2_1{},
		UnpackagedFiles: map[spdx.ElementID]*spdx.File2_1{},
		Relationships:   []*spdx.Relationship2_1{},
		CreationInfo: &spdx.CreationInfo2_1{
			SPDXVersion:    "SPDX-2.1",
			DataLicense:    "CC0-1.0",
			SPDXIdentifier: spdx.ElementID("DOCUMENT"),
			DocumentName:   app.ImageName,
			// TODO: generate a correct namespace
			DocumentNamespace: "https://gitlab.com/swinslow/contcompl/spdxDoc",
			CreatorTools:      []string{"gitlab.com/swinslow/contcompl"},
			Created:           created,
		},
	}

	for _, pkg := range pkgs {
		spdxPkg := createPackage(pkg)
		doc.Packages[spdxPkg.PackageSPDXIdentifier] = spdxPkg
	}

	return doc, nil
}

func createPackage(pkg Package) *spdx.Package2_1 {
	return &spdx.Package2_1{
		PackageName:           pkg.Name,
		PackageSPDXIdentifier: spdx.ElementID(pkg.Name),
		PackageVersion:        pkg.Version,
		// TODO: when getting package info from container, get the right URL(s?) too
		PackageDownloadLocation:   "NOASSERTION",
		FilesAnalyzed:             false,
		IsFilesAnalyzedTagPresent: true,
		PackageLicenseConcluded:   "NOASSERTION",
		PackageLicenseDeclared:    "NOASSERTION",
		PackageCopyrightText:      "NOASSERTION",
	}
}

func saveSPDXDocument(doc *spdx.Document2_1, outPath string) error {
	w, err := os.Create(outPath)
	if err != nil {
		return fmt.Errorf("Error while opening %s for writing: %v", outPath, err)
	}
	defer w.Close()

	err = tvsaver.Save2_1(doc, w)
	if err != nil {
		return fmt.Errorf("Error while saving SPDX document to %s: %v", outPath, err)
	}

	return nil
}
