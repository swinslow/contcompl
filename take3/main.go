// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"github.com/mholt/archiver/v3"
)

type AppDetails struct {
	// working container name
	ContainerId string

	// final image name
	ImageName string

	// name and version for base image
	Base    string
	Version string

	// which packages to install
	Pkgs []string

	// local directory with my app's sources
	AppSrc string

	// container directory where sources should be placed
	AppDst string

	// entrypoint command
	AppEntrypoint string

	// local directory where sources should be copied
	OutputDir string
}

func main() {

	// ===== configuration variables =====

	app := AppDetails{
		ContainerId: "contcompl-working",
		ImageName:   "myapp",
		Base:        "debian",
		Version:     "sid-slim",
		Pkgs: []string{
			"sl",
		},
		AppSrc:        "./app",
		AppDst:        "/app",
		AppEntrypoint: "/app/app.sh",
		OutputDir:     "./output",
	}

	// ===== build steps =====

	// create local directory to output sources
	log.Printf("creating output directory %s", app.OutputDir)
	err := os.MkdirAll(app.OutputDir, 0755)
	if err != nil {
		log.Fatalf("error creating output directory: %v", err)
	}

	// start with base image
	if app.Version == "" {
		log.Printf("setting up base image %s for container %s", app.Base, app.ContainerId)
	} else {
		log.Printf("setting up base image %s:%s for container %s", app.Base, app.Version, app.ContainerId)
	}
	gotId, err := buildahFrom(app.ContainerId, app.Base, app.Version)
	if err != nil {
		log.Fatalf("error setting up base image: %v", err)
	}
	if gotId != app.ContainerId {
		log.Fatalf("got different container ID than requested: wanted %s, got %s; bailing\n", app.ContainerId, gotId)
	}

	// update / upgrade packages -- TODO: is this actually needed?
	log.Printf("running apt update in container")
	_, err = buildahRun(app.ContainerId, "apt", []string{"update", "-y"}...)
	if err != nil {
		log.Fatalf("error running apt update: %v", err)
	}
	log.Printf("running apt upgrade in container")
	_, err = buildahRun(app.ContainerId, "apt", []string{"upgrade", "-y"}...)
	if err != nil {
		log.Fatalf("error running apt upgrade: %v", err)
	}

	// install requested packages
	log.Printf("running apt install on requested packages")
	_, err = buildahRun(app.ContainerId, "apt", []string{"install", "-y", strings.Join(app.Pkgs, " ")}...)
	if err != nil {
		log.Fatalf("error running apt install: %v", err)
	}

	// copy get-sources script into the working container
	log.Printf("copying get-sources.sh script into container")
	err = buildahCopy(app.ContainerId, "./get-sources.sh", "/root/get-sources.sh")
	if err != nil {
		log.Fatalf("error copying get-sources.sh script into container: %v", err)
	}

	// run it
	log.Printf("running get-sources.sh script in container")
	_, err = buildahRun(app.ContainerId, "/root/get-sources.sh")
	if err != nil {
		log.Fatalf("error running get-sources script: %v", err)
	}

	// remove get-sources script
	log.Printf("removing get-sources.sh script from container")
	_, err = buildahRun(app.ContainerId, "rm", "/root/get-sources.sh")
	if err != nil {
		log.Fatalf("error removing get-sources script: %v", err)
	}

	// mount the container, copy out the sources tarball and packages list
	// via unshare so that we can do it rootless
	log.Printf("retrieving sources and packages list from container")
	unshareCmd := "sh"
	unshareArgs := []string{"-c", fmt.Sprintf("cp ${MOUNTPOINT}/root/* %s", app.OutputDir)}
	err = buildahUnshare(app.ContainerId, unshareCmd, unshareArgs...)
	if err != nil {
		log.Fatalf("error unsharing to retrieve sources: %v", err)
	}

	// remove sources and packages list from container
	log.Printf("removing sources and packages list from container")
	_, err = buildahRun(app.ContainerId, "rm", "/root/sources.tar.gz", "/root/packages.list")
	if err != nil {
		log.Fatalf("error removing sources and packages list: %v", err)
	}

	// copy our own app's sources into the container
	log.Printf("copying app sources into container")
	err = buildahCopy(app.ContainerId, app.AppSrc, app.AppDst)
	if err != nil {
		log.Fatalf("error copying app sources into container: %v", err)
	}

	// also compress and copy app's sources into sources dir
	log.Printf("compressing and copying app sources into output")
	err = archiver.Archive([]string{app.AppSrc}, path.Join(app.OutputDir, "app.tar.gz"))
	if err != nil {
		log.Fatalf("error compressing and copying app sources: %v", err)
	}

	// configure the entrypoint
	log.Printf("configuring entrypoint in container")
	err = buildahConfig(app.ContainerId, app.AppEntrypoint)
	if err != nil {
		log.Fatalf("error configuring entrypoint: %v", err)
	}

	// and finalize the container
	err = buildahCommit(app.ContainerId, app.ImageName)
	if err != nil {
		log.Fatalf("error finalizing container: %v", err)
	}

	// also make and save SPDX doc
	doc, err := makeSPDXDocument(app)
	if err != nil {
		log.Printf("unable to create SPDX document: %v", err)
	}
	spdxPath := path.Join(app.OutputDir, "app.spdx")
	err = saveSPDXDocument(doc, spdxPath)
	if err != nil {
		log.Printf("unable to save SPDX document to %s: %v", spdxPath, err)
	}

	log.Printf("Completed; image name is %s", app.ImageName)
}
