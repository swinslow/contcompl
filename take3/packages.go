// SPDX-License-Identifier: MIT

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Package struct {
	Name    string
	Version string
}

func getPackages(pkglistPath string) ([]Package, error) {
	pkgs := []Package{}

	// open the file
	r, err := os.Open(pkglistPath)
	if err != nil {
		return nil, fmt.Errorf("Error while opening package list file %s for reading: %v", pkglistPath, err)
	}
	defer r.Close()

	// scan through line by line
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "=")
		if len(parts) != 2 {
			return nil, fmt.Errorf("Error while parsing package list file %s: in line %s, expected 1 '=' but got %d",
				pkglistPath, line, len(parts)-1)
		}
		pkgs = append(pkgs, Package{Name: parts[0], Version: parts[1]})
	}
	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("Error while parsing package list file %s: %v", pkglistPath, err)
	}

	return pkgs, nil
}
