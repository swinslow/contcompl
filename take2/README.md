SPDX-License-Identifier: MIT

Second attempt
==============

- Uses command-line calls to buildah, made from within a Go program

Note that instead of the Go program making command-line calls via
os/exec, I should be calling buildah directly. However, I've been
getting errors when trying to include buildah directly per the
[buildah docs tutorial][tutorial], so for this next attempt I'll
keep using command-line buildah.

[tutorial]: https://github.com/containers/buildah/blob/master/docs/tutorials/04-include-in-your-build-tool.md

Requirements
------------
* [buildah](https://github.com/containers/buildah)
* [mholt/archiver](https://github.com/mholt/archiver)

buildah will need to be installed as a command-line tool (see above).
archiver is used as a Go module.

Limitations
-----------
This version of the proof-of-concept can now work with any version
of the `debian` base image, by setting the `base` and `version`
configuration variables in [`main.go`](./main.go) accordingly.

It also should work with other apt-based distributions derived from
debian, and has been tested and confirmed to work with `ubuntu`.

It doesn't currently work with other base images.

It also doesn't work with Dockerfiles. The "build process," such as
it is, is limited to installing packages using the base image's
package manager, and then copying your own application's sources
into the container.

How to run
----------
1. Edit the configuration variables at the top of [`main.go`](./main.go)
   to configure for your application.

2. Run `go build`

3. Run `./take2`

4. The source code for the installed packages, as well as a list
   of the packages and versions, will be saved in the `output`
   directory (or a different directory depending on your config),
   along with a copy of your application's own sources.

A sample application is included in ./app/app.sh, which is a
bash script that does the critically important task of
displaying a steam locomotive and then saying hi.

DO NOT run the [`get-sources.sh`](./get-sources.sh) script on your own system. It
is copied into and run from within the container as part of
the steps in [`main.go`](./main.go).

