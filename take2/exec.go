// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
	"os/exec"
	"strings"
)

// buildahFrom runs the "buildah from" command, and returns the container ID
// if successful or err if error.
func buildahFrom(containerId string, base string, version string) (string, error) {
	if containerId == "" {
		return "", fmt.Errorf("containerId is empty string")
	}

	if base == "" {
		return "", fmt.Errorf("base is empty string")
	}

	var baseImage string
	if version == "" {
		baseImage = base
	} else {
		baseImage = base + ":" + version
	}

	cmd := exec.Command("buildah", "from", "--name=" + containerId, baseImage)

	bytes, err := cmd.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("error running command: %v: %s", err, string(bytes))
	}

	// take output and get just last line, trimming spaces
	// and handle different systems' newline characters
	fixedNewlines := strings.Replace(string(bytes), "\r\n", "\n", -1)
	lines := strings.Split(strings.TrimSpace(fixedNewlines), "\n")

	return strings.TrimSpace(lines[len(lines)-1]), nil
}

// buildahRun runs the "buildah run" command, and returns the output if
// successful or err if error.
func buildahRun(containerId string, command string, args ...string) (string, error) {
	if containerId == "" {
		return "", fmt.Errorf("containerId is empty string")
	}

	if command == "" {
		return "", fmt.Errorf("command is empty string")
	}

	allArgs := []string{"run", containerId, "--", command}
	allArgs = append(allArgs, args...)

	cmd := exec.Command("buildah", allArgs...)

	bytes, err := cmd.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("error running command: %v: %s", err, string(bytes))
	}

	return string(bytes), nil
}

// buildahCopy runs the "buildah copy" command, and returns nil if succcessful
// or err if error.
func buildahCopy(containerId string, src string, dst string) error {
	if containerId == "" {
		return fmt.Errorf("containerId is empty string")
	}

	if src == "" {
		return fmt.Errorf("src is empty string")
	}

	if dst == "" {
		return fmt.Errorf("dst is empty string")
	}

	cmd := exec.Command("buildah", "copy", "--quiet", containerId, src, dst)

	bytes, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("error running command: %v: %s", err, string(bytes))
	}

	return nil
}

// buildahUnshare runs the "buildah unshare" command, and returns nil if
// successful or err if error.
func buildahUnshare(containerId string, command string, args ...string) error {
	if containerId == "" {
		return fmt.Errorf("containerId is empty string")
	}

	if command == "" {
		return fmt.Errorf("command is empty string")
	}

	allArgs := []string{"unshare", "--mount", "MOUNTPOINT="+containerId, command}
	allArgs = append(allArgs, args...)

	cmd := exec.Command("buildah", allArgs...)

	bytes, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("error running command: %v: %s", err, string(bytes))
	}

	return nil
}

// buildahConfig runs the "buildah config" command, and returns nil if
// successful or err if error.
func buildahConfig(containerId string, entrypoint string) error {
	if containerId == "" {
		return fmt.Errorf("containerId is empty string")
	}

	if entrypoint == "" {
		return fmt.Errorf("entrypoint is empty string")
	}

	cmd := exec.Command("buildah", "config", "--entrypoint", entrypoint, containerId)

	bytes, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("error running command: %v: %s", err, string(bytes))
	}

	return nil
}

// buildahCommit runs the "buildah commit" command, and returns nil if
// successful or err if error.
func buildahCommit(containerId string, imageName string) error {
	if containerId == "" {
		return fmt.Errorf("containerId is empty string")
	}

	if imageName == "" {
		return fmt.Errorf("imageName is empty string")
	}

	cmd := exec.Command("buildah", "commit", containerId, imageName)

	bytes, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("error running command: %v: %s", err, string(bytes))
	}

	return nil
}

