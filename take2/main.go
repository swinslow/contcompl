// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"github.com/mholt/archiver/v3"
)

func main() {

	// ===== configuration variables =====

	// working container name
	containerId := "contcompl-working"

	// final image name
	imageName := "myapp"

	// name and version for base image
	base := "debian"
	version := "sid-slim"

	// which packages to install
	pkgs := []string{
		"sl",
	}

	// local directory with my app's sources
	appSrc := "./app"

	// container directory where sources should be placed
	appDst := "/app"

	// entrypoint command
	appEntrypoint := "/app/app.sh"

	// local directory where sources should be copied
	outputDir := "./output"

	// ===== build steps =====

	// create local directory to output sources
	log.Printf("creating output directory %s", outputDir)
	err := os.MkdirAll(outputDir, 0755)
	if err != nil {
		log.Fatalf("error creating output directory: %v", err)
	}

	// start with base image
	if version == "" {
		log.Printf("setting up base image %s for container %s", base, containerId)
	} else {
		log.Printf("setting up base image %s:%s for container %s", base, version, containerId)
	}
	gotId, err := buildahFrom(containerId, base, version)
	if err != nil {
		log.Fatalf("error setting up base image: %v", err)
	}
	if gotId != containerId {
		log.Fatalf("got different container ID than requested: wanted %s, got %s; bailing\n", containerId, gotId)
	}

	// update / upgrade packages -- TODO: is this actually needed?
	log.Printf("running apt update in container")
	_, err = buildahRun(containerId, "apt", []string{"update", "-y"}...)
	if err != nil {
		log.Fatalf("error running apt update: %v", err)
	}
	log.Printf("running apt upgrade in container")
	_, err = buildahRun(containerId, "apt", []string{"upgrade", "-y"}...)
	if err != nil {
		log.Fatalf("error running apt upgrade: %v", err)
	}

	// install requested packages
	log.Printf("running apt install on requested packages")
	_, err = buildahRun(containerId, "apt", []string{"install", "-y", strings.Join(pkgs, " ")}...)
	if err != nil {
		log.Fatalf("error running apt install: %v", err)
	}

	// copy get-sources script into the working container
	log.Printf("copying get-sources.sh script into container")
	err = buildahCopy(containerId, "./get-sources.sh", "/root/get-sources.sh")
	if err != nil {
		log.Fatalf("error copying get-sources.sh script into container: %v", err)
	}

	// run it
	log.Printf("running get-sources.sh script in container")
	_, err = buildahRun(containerId, "/root/get-sources.sh")
	if err != nil {
		log.Fatalf("error running get-sources script: %v", err)
	}

	// remove get-sources script
	log.Printf("removing get-sources.sh script from container")
	_, err = buildahRun(containerId, "rm", "/root/get-sources.sh")
	if err != nil {
		log.Fatalf("error removing get-sources script: %v", err)
	}

	// mount the container, copy out the sources tarball and packages list
	// via unshare so that we can do it rootless
	log.Printf("retrieving sources and packages list from container")
	unshareCmd := "sh"
	unshareArgs := []string{"-c", fmt.Sprintf("cp ${MOUNTPOINT}/root/* %s", outputDir)}
	err = buildahUnshare(containerId, unshareCmd, unshareArgs...)
	if err != nil {
		log.Fatalf("error unsharing to retrieve sources: %v", err)
	}

	// remove sources and packages list from container
	log.Printf("removing sources and packages list from container")
	_, err = buildahRun(containerId, "rm", "/root/sources.tar.gz", "/root/packages.list")
	if err != nil {
		log.Fatalf("error removing sources and packages list: %v", err)
	}

	// copy our own app's sources into the container
	log.Printf("copying app sources into container")
	err = buildahCopy(containerId, appSrc, appDst)
	if err != nil {
		log.Fatalf("error copying app sources into container: %v", err)
	}

	// also compress and copy app's sources into sources dir
	log.Printf("compressing and copying app sources into output")
	err = archiver.Archive([]string{appSrc}, path.Join(outputDir, "app.tar.gz"))
	if err != nil {
		log.Fatalf("error compressing and copying app sources: %v", err)
	}

	// configure the entrypoint
	log.Printf("configuring entrypoint in container")
	err = buildahConfig(containerId, appEntrypoint)
	if err != nil {
		log.Fatalf("error configuring entrypoint: %v", err)
	}

	// and finalize the container
	err = buildahCommit(containerId, imageName)
	if err != nil {
		log.Fatalf("error finalizing container: %v", err)
	}

	log.Printf("Completed; image name is %s", imageName)
}

