module gitlab.com/swinslow/contcompl

go 1.14

require (
	github.com/mholt/archiver/v3 v3.3.0
	github.com/spdx/tools-golang v0.0.0-20200525210420-feb87345dfd6
)
